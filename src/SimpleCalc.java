// Demonstrates JPanel, GridLayout and a few additional useful graphical features.
// adapted from an example by john ramirez (cs prof univ pgh)
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.*;

public class SimpleCalc
{
	JFrame window;  // the main window which contains everything
	Container content ;
	JButton[] digits = new JButton[12]; 
	JButton[] ops = new JButton[4];
	JTextField expression;
	JButton equals;
	JTextField result;
	
	public SimpleCalc()
	{
		window = new JFrame( "Simple Calc");
		content = window.getContentPane();
		content.setLayout(new GridLayout(2,1)); // 2 row, 1 col
		ButtonListener listener = new ButtonListener();
		
		// top panel holds expression field, equals sign and result field  
		// [4+3/2-(5/3.5)+3]  =   [3.456]
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(1,3)); // 1 row, 3 col
		
		expression = new JTextField();
		expression.setFont(new Font("verdana", Font.BOLD, 16));
		expression.setText("");
		
		equals = new JButton("=");
		equals.setFont(new Font("verdana", Font.BOLD, 20 ));
		equals.addActionListener( listener ); 
		
		result = new JTextField();
		result.setFont(new Font("verdana", Font.BOLD, 16));
		result.setText("");
		
		topPanel.add(expression);
		topPanel.add(equals);
		topPanel.add(result);
						
		// bottom panel holds the digit buttons in the left sub panel and the operators in the right sub panel
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(1,2)); // 1 row, 2 col
	
		JPanel  digitsPanel = new JPanel();
		digitsPanel.setLayout(new GridLayout(4,3));	
		
		for (int i=0 ; i<10 ; i++ )
		{
			digits[i] = new JButton( ""+i );
			digitsPanel.add( digits[i] );
			digits[i].addActionListener( listener ); 
		}
		digits[10] = new JButton( "C" );
		digitsPanel.add( digits[10] );
		digits[10].addActionListener( listener ); 

		digits[11] = new JButton( "CE" );
		digitsPanel.add( digits[11] );
		digits[11].addActionListener( listener ); 		
	
		JPanel opsPanel = new JPanel();
		opsPanel.setLayout(new GridLayout(4,1));
		String[] opCodes = { "+", "-", "*", "/" };
		for (int i=0 ; i<4 ; i++ )
		{
			ops[i] = new JButton( opCodes[i] );
			opsPanel.add( ops[i] );
			ops[i].addActionListener( listener ); 
		}
		bottomPanel.add( digitsPanel );
		bottomPanel.add( opsPanel );
		
		content.add( topPanel );
		content.add( bottomPanel );
	
		window.setSize( 640,480);
		window.setVisible( true );
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// We are again using an inner class here so that we can access
	// components from within the listener.  Note the different ways
	// of getting the int counts into the String of the label
	
	class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component whichButton = (Component) e.getSource();
			// how to test for which button?
			// this is why our widgets are 'global' class members
			// so we can refer to them in here
			
			for (int i=0 ; i<10 ; i++ )
				if (whichButton == digits[i])
					expression.setText( expression.getText() + i );
			for (int i=0 ; i<4 ; i++ )
				if (whichButton == ops[i])
					expression.setText(expression.getText() + ops[i].getText());
			try {
				if (whichButton.equals(equals))
					result.setText(eval(expression.getText()));
			} 
			catch (Exception e2) {
				result.setText("#ERROR#");
			}
			if (whichButton.equals(digits[10])) {
				expression.setText("");
				result.setText("");
			}
			if (whichButton.equals(digits[11])) {
				expression.setText(expression.getText().substring(0,expression.getText().length() - 1));
				result.setText("");
			}
					
			// need to add tests for other controls that may have been
			// click that got us in here. Write code to handle those
			
			
		}
		
		String eval(String equation) {
			ArrayList<String> operatorList = new ArrayList<String>();
			ArrayList<Double> operandList = new ArrayList<Double>();
			StringTokenizer st = new StringTokenizer( equation,"+-*/", true );
			
			if(equation.contains("/0"))
				return "#ERROR#";
			
			while (st.hasMoreTokens()) {
				String token = st.nextToken();
				if ("+-/*".contains(token))
					operatorList.add(token);
				else
					operandList.add(Double.parseDouble(token));
	    		}
			
				while (operatorList.contains("*") || operatorList.contains("/")) {
					for (int i = 0; i < operatorList.size(); i++) {
						if (operatorList.get(i).equals("*")) {
							operandList.set(i, operandList.get(i) * operandList.get(i + 1));
							operandList.remove(i + 1);
							operatorList.remove(i);
						}

						else if (operatorList.get(i).equals("/")) {
							operandList.set(i, operandList.get(i) / operandList.get(i + 1));
							operandList.remove(i + 1);
							operatorList.remove(i);
						}
					} 
				}
				
				while (operatorList.contains("+") || operatorList.contains("-")) {
					for (int i = 0; i < operatorList.size(); i++) {
						if (operatorList.get(i).equals("+")) {
							operandList.set(i, operandList.get(i) + operandList.get(i + 1));
							operandList.remove(i + 1);
							operatorList.remove(i);
						}
					
						else if (operatorList.get(i).equals("-")) {
							operandList.set(i, operandList.get(i) - operandList.get(i + 1));
							operandList.remove(i + 1);
							operatorList.remove(i);
						}
					}
				}
				
				return Double.toString(operandList.get(0));
		}
	}

	public static void main(String [] args)
	{
		new SimpleCalc();
	}
}
